require 'json'
require 'open-uri'
require 'ostruct'
require 'testing1/version'


module Testing1
  class Weather
    # TODO: поймать

    def weather(city)
      string = open("https://www.metaweather.com/api/location/search/?query=#{city}").read
      # [{"title":"Moscow","location_type":"City","woeid":2122265,"latt_long":"55.756950,37.614971"}]
      data = JSON.parse(string)

      if city_data = data.first
        woeid = city_data.fetch("woeid")
        title = city_data.fetch("title")

        # https://www.metaweather.com/api/location/44418/
        string = open("https://www.metaweather.com/api/location/#{woeid}/").read
        data = JSON.parse(string)
        today_data = data.fetch("consolidated_weather").first

        {
          city: title,
          min: today_data.fetch("min_temp"),
          max: today_data.fetch("max_temp"),
          state: today_data.fetch("weather_state_name"),
          humidity: today_data.fetch("humidity")
        }
      end
    end
  end
end
