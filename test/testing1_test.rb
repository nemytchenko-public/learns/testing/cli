require 'test_helper'

class Testing1Test < Minitest::Test
  def test_that_it_has_a_version_number
    refute_nil ::Testing1::VERSION
  end

  def test_it_shows_weather
    WebMock.disable_net_connect!

    stub_request(:get, 'https://www.metaweather.com/api/location/search/?query=moscow')
      .to_return(
        body: '[{"title":"Moscow","location_type":"City","woeid":2122265,"latt_long":"55.756950,37.614971"}]',
      )
    stub_request(:get, "https://www.metaweather.com/api/location/2122265/")
      .to_return(
        body: '{"consolidated_weather":[{"id":5004478969282560,"weather_state_name":"Heavy Cloud","weather_state_abbr":"hc","wind_direction_compass":"WNW","created":"2018-10-13T09:07:46.060708Z","applicable_date":"2018-10-13","min_temp":7.42,"max_temp":14.9775,"the_temp":13.11,"wind_speed":5.445333186576394,"wind_direction":281.8974858210367,"air_pressure":1025.715,"humidity":78,"visibility":13.069300286327845,"predictability":71},{"id":6108091678457856,"weather_state_name":"Heavy Cloud","weather_state_abbr":"hc","wind_direction_compass":"WNW","created":"2018-10-13T09:07:49.500889Z","applicable_date":"2018-10-14","min_temp":7.875,"max_temp":17.0675,"the_temp":14.56,"wind_speed":5.329386614556987,"wind_direction":296.87985873686824,"air_pressure":1023.2,"humidity":78,"visibility":12.074174321959756,"predictability":71}]}'
      )

    expected_data = {
      city: "Moscow",
      min: 7.42,
      max: 14.9775,
      state: "Heavy Cloud",
      humidity: 78
    }
    assert { Testing1::Weather.new.weather('moscow') == expected_data }
  end

  # def test_it_shows_error_message

  # end
end
